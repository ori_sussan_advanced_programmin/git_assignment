#ifndef QUEUE_H
#define QUEUE_H

/* a queue contains positive integer values. */
typedef struct queue
{
	int _count;
	int _maxSize;
	int *_elements;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
bool isEmpty(queue *q);
bool isFull(queue *q);
void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

void initQueue(queue* q, unsigned int size)
{
	q->_maxSize = size;
	q->_count = 0;
	q->_elements = new int[size];
}

void cleanQueue(queue* q)
{
	delete[] q->_elements;
}

bool isEmpty(queue *q)
{
	return (q->_count == 0);
}

bool isFull(queue *q)
{
	return(q->_count == q->_maxSize);
}

void enqueue(queue* q, unsigned int newValue)
{
	if (!isFull(q))
	{
		q->_elements[q->_count] = newValue;
		q->_count++;
	}
}

int dequeue(queue* q)
{
	int ans = -1;
	int i = 0, j = 0;
	if (!isEmpty(q))
	{
		ans = q->_elements[i];
		for (j = 1, i = 0; j < q->_maxSize; i++, j++)
		{
			q->_elements[i] = q->_elements[j];
		}
	}
	return ans;
}

#endif /* QUEUE_H */